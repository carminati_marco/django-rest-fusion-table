# README #
### Explanation ###
Single page app with a full viewport map on top and a list of addresses below based on Django 1.9.
Use case: If I click any location on the map: validate that this has a real address and it’s not some wood/mountain/ocean, etc. 
If valid: save it to a simple db table with lat, lon, address (can be single string) and also to google fusion tables (decide what data). 
Have a marker appear instantly after the click on the map based on the google fusion table data. 
Update the list of addresses underneath the map with the location where you clicked. 
Duplicates on google fusion table are not allowed. 
I can reset all data on google fusion tables and the database and start fresh

### Installation ###
NOTE: Create a virtualenv and activate it before intalling. See
create a new Virtualenv [(install virtualenv)](https://virtualenv.pypa.io/en/stable/installation/)

```
git clone https://carminati_marco@bitbucket.org/carminati_marco/rest_fusion_table.git
cd rest_fusion_table
pip install -r requirements.txt
python manage.py migrate
python manage.py runserver
```

### How clear my data ###
Go to
```
http://127.0.0.1:8000/locations/api/delete/
```
and clear markers you've added!

### Who do I talk to? ###
* carminati.marco@gmail.com

