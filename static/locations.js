var map;
function initMap() {
    map = new google.maps.Map(document.getElementById('map'), {
      zoom: 2,
      center: new google.maps.LatLng(2.8,-187.3),
      mapTypeId: 'terrain'
    });

    map.addListener('click', function(e) {
        placeMarkerAndPanTo(e.latLng, map);
    });

    $.getJSON( "/locations/api/?format=json", function(data) {
        $.each( data, function( key, val ) {
            var latLng = new google.maps.LatLng(val.lat, val.lon);

            var marker = new google.maps.Marker ({
              position: latLng,
              map: map
            });
        });
    });
}

function placeMarkerAndPanTo(latLng, map) {
    $.ajax({
      type: "POST",
      url: "/locations/api/",
      data: {'lat': latLng.lat(), 'lon': latLng.lng() },
      success: function(response) {
            if (response.status == '201') {
                var marker = new google.maps.Marker({
                  position: latLng,
                  map: map
                });
                map.panTo(latLng);
            } else {
                alert('Address not found!')
            }
      },
      dataType: "json"
    });
}