from django.http import HttpResponseRedirect
from oauth2client.client import FlowExchangeError

from .models import WebServerFlow

WEB_SERVER_FLOW = WebServerFlow()


def check_code(func):
    """
    Checks if 'code' for OAuth2WebServerFlow is in session and valid.
    if not it redirects to obtain it.
    """

    def _wrapper(request):

        code = request.GET.get('code', False)
        if not code:
            # redirect as I haven't code.

            return HttpResponseRedirect(WEB_SERVER_FLOW.get_authorize_url())
        else:
            try:
                request.session['code'] = code  # save in session.
                request.session['credentials'] = WEB_SERVER_FLOW.get_credentials(code)
            except Exception:
                # clear + redirect as I have wrong code.
                if 'code' in request.session:
                    del request.session['code']
                if 'service' in request.session:
                    del request.session['service']
                return HttpResponseRedirect(WEB_SERVER_FLOW.get_authorize_url())

        return func(request)

    return _wrapper
