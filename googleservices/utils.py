from django.conf import settings

from .models import WebServerFlow

WEB_SERVER_FLOW = WebServerFlow()


def delete_all_rows(credentials):
    """ 
    deletes row in Fusion Table. 
    """
    service = WEB_SERVER_FLOW.get_service_fusiontable(credentials)
    service.query().sql(sql="DELETE FROM %(FUSION_TABLE_ID)s" %
                            dict(FUSION_TABLE_ID=settings.FUSION_TABLE_ID)).execute()


def insert_row(credentials, address, text):
    """
    creates a row in Fusion Table.
    """

    # NOTE: Add a check if the table is used by other sources!
    service = WEB_SERVER_FLOW.get_service_fusiontable(credentials)
    service.query().sql(sql="INSERT INTO %(FUSION_TABLE_ID)s (Location, Text) VALUES('%(address)s', '%(text)s')"
                            % dict(FUSION_TABLE_ID=settings.FUSION_TABLE_ID, address=address, text=text)).execute()
