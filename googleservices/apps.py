from __future__ import unicode_literals

from django.apps import AppConfig


class GoogleservicesConfig(AppConfig):
    name = 'googleservices'
