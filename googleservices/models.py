from __future__ import unicode_literals

import httplib2
from apiclient.discovery import build
from django.conf import settings
from oauth2client.client import OAuth2WebServerFlow, OAuth2Credentials


class Singleton(type):
    _instances = {}

    def __call__(cls, *args, **kwargs):
        if cls not in cls._instances:
            cls._instances[cls] = super(Singleton, cls).__call__(*args, **kwargs)
        return cls._instances[cls]


class WebServerFlow(metaclass=Singleton):
    """
    Creates a class as Singleton (one istance for session).
    """
    flow = OAuth2WebServerFlow(**settings.GOOGLE_SETTINGS_WEBSERVER)
    service_fusiontable = None

    def get_authorize_url(self):
        """       
        gets Uri for Google's step1.
        """
        auth_uri = self.flow.step1_get_authorize_url()
        return auth_uri

    def get_credentials(self, code):
        """       
        gets Credential for Google's step1.
        """
        credentials = self.flow.step2_exchange(code)
        return credentials.to_json()  # to_json as it'll be saved in session

    def _get_service(self, credentials_json, service, version):
        """
        builds and creates Google's service.
        """
        credentials = OAuth2Credentials.from_json(credentials_json)
        http = credentials.authorize(httplib2.Http())
        service = build(service, version, http=http)
        return service

    def get_service_fusiontable(self, credentials):
        """
        creates f(x) for fusiontable. 
        """
        if not self.service_fusiontable:
            self.service_fusiontable = self._get_service(credentials, 'fusiontables', 'v2')
        return self.service_fusiontable
