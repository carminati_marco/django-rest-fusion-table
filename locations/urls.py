from django.conf.urls import url

from .views import MarkerCreateReadView, MarkerDeletedView

urlpatterns = [
    url(r'^api/$', MarkerCreateReadView.as_view({'get': 'list', 'post': 'create'})),
    url(r'^api/delete/$', (MarkerDeletedView.as_view())),
]
