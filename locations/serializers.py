from rest_framework import serializers

from .models import Marker


class MarkerSerializer(serializers.ModelSerializer):
    """
    Serializer for Marker.
    """
    class Meta:
        model = Marker
        fields = ('lat', 'lon', 'address')
