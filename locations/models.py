from __future__ import unicode_literals

from django.db import models


class Marker(models.Model):
    """
    Simple class Marker.
    """
    lat = models.DecimalField(max_digits=9, decimal_places=6)
    lon = models.DecimalField(max_digits=9, decimal_places=6)
    address = models.CharField(max_length=300)

    class Meta:
        unique_together = ('lat', 'lon',)
