from django.core.exceptions import ObjectDoesNotExist
from django.http import JsonResponse
from django.views.generic import View
from rest_framework import status
from rest_framework.viewsets import ModelViewSet

from googleservices.utils import delete_all_rows
from .models import Marker
from .serializers import MarkerSerializer
from .utils import create_marker


class MarkerCreateReadView(ModelViewSet):
    """
    View to obtain list + create markers
    """
    serializer_class = MarkerSerializer
    queryset = Marker.objects.all()

    def create(self, serializer):

        lat, lon = serializer.data['lat'], serializer.data['lon']  # get lat/lon.

        try:
            Marker.objects.get(lat=lat, lon=lon)
            created = False
        except ObjectDoesNotExist:
            created = create_marker(lat, lon,
                                    self.request.session.get('credentials')) is not None

        response_status = status.HTTP_201_CREATED if created else status.HTTP_304_NOT_MODIFIED
        return JsonResponse(dict(status=response_status))


class MarkerDeletedView(View):
    def get(self, request):

        # check if I've credential
        if 'credentials' in request.session:
            Marker.objects.all().delete()
            delete_all_rows(request.session.get('credentials'))
            response = dict(status=status.HTTP_200_OK)
        else:
            response = dict(status=status.HTTP_511_NETWORK_AUTHENTICATION_REQUIRED,
                            message="Please, go to http://127.0.0.1:8000 to obtain credentials")

        return JsonResponse(response)
