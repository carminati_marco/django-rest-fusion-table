import requests

from googleservices.utils import insert_row
from .models import Marker

GOOGLE_API_URL = "https://maps.googleapis.com/maps/api/geocode/json?latlng=%(lat)s,%(lon)s&key=AIzaSyDaZaen_avMZfZ7S8KgfIbVN1u_Zft_FPs"

TYPE_STREET_ADDRESS = 'street_address'
TYPE_ROUTE = 'route'


def create_marker(lat, lon, credentials):
    """
    F(x) to create a Marker using lat and lon.
    :return: marker, if created
    """

    # calls GOOGLE_API_URL to obtain results
    response = requests.get(GOOGLE_API_URL % dict(lat=lat, lon=lon))
    json = response.json()

    if 'results' in json:
        for result in json['results']:
            # if it's an address or route, I can process.
            if TYPE_STREET_ADDRESS in result['types'] or TYPE_ROUTE in result['types']:
                address = result['formatted_address']

                # creates marker.
                marker = Marker.objects.create(address=address, lat=lat, lon=lon)

                # insert row in google fusion table.
                text = '%(lat)s;%(lon)s' % dict(lat=lat, lon=lon)
                # as marker is created, it's sure that the row in fusion table isn't duplicate
                insert_row(credentials, address, text)

                return marker

    return None
